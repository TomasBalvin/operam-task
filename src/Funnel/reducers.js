import { SHOW_TOOLTIP, HIDE_TOOLTIP, CHANGE_TOOLTIP_INDEX } from './actions';

const defaultFunnel = {
  tooltipVisible: false,
  tooltipIndex: 0
};

export const funnelReducer = (state = defaultFunnel, action) => {
  switch (action.type) {
    case SHOW_TOOLTIP:
      return {
        ...state,
        tooltipVisible: true
      };
    case HIDE_TOOLTIP:
      return {
        ...state,
        tooltipVisible: false
      };
    case CHANGE_TOOLTIP_INDEX:
      return {
        ...state,
        tooltipIndex: action.index
      };
    default:
      return state;
  }
};
