import React from 'react';
import { connect } from 'react-redux';

import { hideTooltip, showTooltip, changeTooltipIndex } from '../actions';

const Tooltip = ({
  width,
  height,
  lineX,
  circles,
  tooltipVisible,
  tooltipIndex,
  handleMouseEnter,
  handleMouseLeave,
  handleMouseMove
}) => {
  const circlesNodes = [];
  if (tooltipVisible) {
    circles.forEach((circle) => {
      const {x, y, offset, values, color} = circle;

      if (tooltipIndex >= offset && tooltipIndex < values.length) {
        circlesNodes.push(
          <circle
            key={color}
            r={5.5}
            cx={x(tooltipIndex - offset)}
            cy={y(values[tooltipIndex - offset])}
            stroke={color}
            fill="white"
            strokeWidth="3px"
          />
        );
      }
    });
  }

  return (
    <g>
      {tooltipVisible &&
        <line transform={`translate(${lineX(tooltipIndex)})`} y2={height} stroke="black" />
      }
      {circlesNodes}
      <rect
        width={width}
        height={height}
        fill={'transparent'}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onMouseMove={handleMouseMove}
      />
    </g>
  );
};

const mapStateToProps = (state) => ({
  tooltipVisible: state.funnel.tooltipVisible,
  tooltipIndex: state.funnel.tooltipIndex
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  handleMouseEnter: () => dispatch(showTooltip()),
  handleMouseLeave: () => dispatch(hideTooltip()),
  handleMouseMove: ({ nativeEvent }) => {
    const x = nativeEvent.offsetX - ownProps.offsetLeft;
    const index = Math.round(ownProps.lineX.invert(x));
    dispatch(changeTooltipIndex(index));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Tooltip);
