import React from 'react';

import { area } from 'd3';

const Area = ({ x, y, values, color, fill }) => {
  const areaGenerator = area()
    .x((value, index) => x(index))
    .y1(value => y(value))
    .y0(y(0));

  const topLine = areaGenerator.lineY1();

  return (
    <g>
      <path d={topLine(values)} stroke={color} strokeWidth="3px" />
      <path d={areaGenerator(values)} fill={fill} />
    </g>
  );
};

export default Area;
