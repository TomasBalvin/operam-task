import React from 'react';
import ReactDOM from 'react-dom';
import { select } from 'd3';

import './Grid.styl';

class Grid extends React.Component {
  componentDidMount() {
    this.renderGrid();
  }

  componentDidUpdate() {
    this.renderGrid();
  }

  renderGrid() {
    const node = ReactDOM.findDOMNode(this);
    select(node).call(this.props.grid);
  }

  render() {
    return (
      <g className="Grid" />
    );
  }
}

export default Grid;
