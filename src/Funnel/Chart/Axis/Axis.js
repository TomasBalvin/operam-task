import React from 'react';
import ReactDOM from 'react-dom';
import { select } from 'd3';

import './Axis.styl';

class Axis extends React.PureComponent {
  componentDidMount() {
    this.renderAxis();
  }

  componentDidUpdate() {
    this.renderAxis();
  }

  renderAxis() {
    const node = ReactDOM.findDOMNode(this);
    select(node).call(this.props.axis);
    if (this.props.type === 'top') {
      select(node).selectAll('.tick text').call(this.wrap, this.props.bandwidth);
    }
  }


  wrap(text, width) {
    text.each(function() {
      const text = select(this);
      const words = text.text().split(/\s+/).reverse();
      let word;
      let line = [];
      let lineNumber = 0;
      let lineHeight = 1.1;
      let y = text.attr('y');
      let dy = parseFloat(text.attr('dy'));
      let tspan = text.text(null).append('tspan').attr('x', 0).attr('y', y).attr('dy', `${dy}em`);

      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(' '));
        if (tspan.node().getComputedTextLength() > width - 20) {
          line.pop();
          tspan.text(line.join(' '));
          line = [word];
          tspan = text.append('tspan').attr('x', 0).attr('y', y).attr('dy', `${++lineNumber * lineHeight + dy}em`).text(word);
        }
      }
      text.attr('dy', `${-lineNumber * lineHeight}em`);
    });
  }

  render() {
    const { type, height } = this.props;

    return (
      <g
        className={`Axis ${type === 'top' ? 'Axis--top' : 'Axis--bottom'}`}
        transform={type === 'bottom' ? `translate(0, ${height})` : ''}
      />
    );
  }
}

export default Axis;
