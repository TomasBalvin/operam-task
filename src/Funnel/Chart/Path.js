import React from 'react';

import { line, curveCardinal } from 'd3';

const Path = ({ x, y, values, color }) => {
  const path = line()
    .x((value, index) => x(index))
    .y(value => y(value))
    .curve(curveCardinal);

  return (
    <path d={path(values)} stroke={color} strokeWidth="2px" />
  );
};

export default Path;
