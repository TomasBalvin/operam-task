import React from 'react';
import { extent, scaleLinear, scalePoint, axisBottom, axisTop } from 'd3';

import './Chart.styl';

import Axis from './Axis/Axis';
import Grid from './Grid/Grid';
import Path from './Path';
import Area from './Area';
import Tooltip from './Tooltip';

import { periodNames, graphGroups } from '../chartData';

class Chart extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      width: 1000,
      height: 562.5,
      offsetLeft: 0
    };
  }

  componentDidMount() {
    this.setWidth();
    window.addEventListener('resize', this.setWidth);
  }

  setWidth = () => {
    const chartComputedStyle = getComputedStyle(this.chart);
    const width = parseInt(chartComputedStyle.width, 10);
    this.setState({
      width: width,
      height: width / 2.5,
      offsetLeft: parseInt(chartComputedStyle.paddingLeft, 10)
    });
  };

  render() {
    const { width, height } = this.state;

    const getBottomAxis = () => {
      let tickValues = [];
      for (let i = periodNames.length - 1; i >= 0; i--) {
        tickValues.push(i);
      }
      tickValues.push('B');

      const scale = scalePoint()
        .domain(tickValues)
        .range([0, width]);

      return axisBottom(scale).tickSize(0).tickPadding(8);
    };

    const getTopAxis = () => {
      const scale = scaleLinear()
        .domain([0, periodNames.length])
        .range([0, width]);

      return axisTop(scale).tickSize(0).tickPadding(16).tickFormat((d) => periodNames[d]);
    };

    const getGrid = () => {
      const scale = scaleLinear()
        .domain([periodNames.length, 0])
        .range([0, width]);

      return axisBottom(scale).tickSize(height).tickFormat('');
    };

    const lines = [];
    const areas = [];
    const circles = [];

    graphGroups.forEach((group) => {
      let valuesToScale = [];
      Object.values(group).forEach((curve) => {
        valuesToScale = [...valuesToScale, ...curve.values];
      });

      Object.entries(group).forEach(([key, curve]) => {
        const { values, offset = 0 } = curve;
        const xStart = offset * (width / periodNames.length);
        const xEnd = xStart + (values.length - 1) * (width / periodNames.length);

        const x = scaleLinear()
          .domain([0, values.length - 1])
          .range([xStart, xEnd]);

        const y = scaleLinear()
          .domain(extent(valuesToScale))
          .range([height, height * 0.1]);

        circles.push({
          values,
          x,
          y,
          offset,
          color: curve.color
        });

        if (curve.type === 'line') {
          lines.push(
            <Path key={key} x={x} y={y} values={values} color={curve.color} />
          );
        } else if (curve.type === 'area') {
          areas.push(
            <Area key={key} x={x} y={y} values={values} color={curve.color} fill={curve.fill} />
          );
        }
      });
    });

    const tooltipLineX = scaleLinear().domain([0, periodNames.length]).range([0, width]);

    return (
      <svg className="Chart" width="100%" height={height} ref={chart => { this.chart = chart; }}>
        {areas}
        <Grid grid={getGrid()} />
        <Axis height={height} type="bottom" axis={getBottomAxis()} />
        <Axis height={height} type="top" axis={getTopAxis()} bandwidth={width / periodNames.length} />
        {lines}
        <Tooltip
          width={width}
          height={height}
          offsetLeft={this.state.offsetLeft}
          circles={circles}
          lineX={tooltipLineX}
        />
      </svg>
    );
  }
}

export default Chart;
