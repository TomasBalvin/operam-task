import React from 'react';
import { connect } from 'react-redux';

import './Funnel.styl';

import Chart from './Chart/Chart';

import { graphGroups } from './chartData';

const Funnel = ({ tooltipVisible, tooltipIndex }) => {
  const legendItems = [];
  graphGroups.forEach((group) => {
    Object.values(group).forEach((curve) => {
      const { offset = 0, values, color, label } = curve;
      legendItems.push(
        <div key={label} className="Funnel-legendItem">
          <div className="Funnel-legendDot" style={{ background: color }} />
          <span>{label}</span>
          {tooltipVisible && tooltipIndex >= offset && tooltipIndex < values.length + offset &&
            <span className="Funnel-legendValue">{values[tooltipIndex - offset]}</span>
          }
        </div>
      );
    });
  });

  return (
    <div className="Funnel">
      <h1 className="Funnel-title">Campaign Lifetime</h1>
      <div className="Funnel-legend">
        {legendItems}
      </div>
      <Chart width={1000} height={500} />
    </div>
  );
};

const mapStateToProps = (state) => ({
  tooltipVisible: state.funnel.tooltipVisible,
  tooltipIndex: state.funnel.tooltipIndex
});

export default connect(mapStateToProps)(Funnel);
