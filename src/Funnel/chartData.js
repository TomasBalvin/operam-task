const acquisitionValues = [0, 50000, 7000, 5000, 6000, 7000, 5000, 6000, 7000, 5000, 6000, 5600, 5300, 7000, 7000];
const retargetingValues = [50000, 52000, 54000, 56000, 57000, 58000, 61000, 61500, 61500, 62000, 66000, 66000, 67000, 67000];
const cpvValues = [0.010, 0.009, 0.007, 0.005, 0.006, 0.005, 0.005, 0.004, 0.005, 0.004, 0.005, 0.004, 0.002, 0.005, 0.006];
const audienceScoreValues = [100000, 130000, 190000, 200000, 210000, 230000, 250000, 300000, 310000, 320000, 325000, 350000, 500000, 600000, 1200000];
const spentValues = [0, 130000, 190000, 200000, 210000, 230000, 250000, 300000, 410000, 520000, 525000, 550000, 600000, 600000, 1200000];

export const graphGroups = [
  {
    acquisition: {
      values: acquisitionValues,
      color: '#14CCBD',
      type: 'line',
      label: 'Acqusition'
    },
    retargeting: {
      values: retargetingValues,
      color: '#F5498B',
      type: 'line',
      offset: 1,
      label: 'Retargeting'
    }
  },
  {
    cpv: {
      values: cpvValues,
      color: '#43BF4D',
      type: 'line',
      label: 'CPV'
    }
  },
  {
    audienceScore: {
      values: audienceScoreValues,
      color: '#D13913',
      type: 'line',
      label: 'Audience Score'
    },
    spent: {
      values: spentValues,
      color: '#DADFE5',
      fill: '#F7F8F9',
      type: 'area',
      label: 'Spent'
    }
  }
];

export const periodNames = [
  'Trailer',
  ':30s',
  ':30s',
  ':30s',
  ':15s',
  ':15s',
  ':15s',
  ':15s',
  'Content Marketing',
  'Content Marketing',
  'Content Marketing',
  'Content Marketing',
  'Ticketing',
  'Ticketing'
];
