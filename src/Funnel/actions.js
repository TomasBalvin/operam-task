export const SHOW_TOOLTIP = 'SHOW_TOOLTIP';
export const showTooltip = () => ({
  type: SHOW_TOOLTIP
});

export const HIDE_TOOLTIP = 'HIDE_TOOLTIP';
export const hideTooltip = () => ({
  type: HIDE_TOOLTIP
});

export const CHANGE_TOOLTIP_INDEX = 'CHANGE_TOOLTIP_INDEX';
export const changeTooltipIndex = (index) => ({
  type: CHANGE_TOOLTIP_INDEX,
  index
});
