import React from 'react';

const BoxOfficeOverview = () => (
  <div className="Dashboard-row">
    <h2 className="Dashboard-rowTitle">Box office</h2>
    <div className="Card Card--rounded Card--success">
      <div className="Overview Overview--boxOffice">
        <div className="Overview-cell">
          <h3 className="Overview-title">Goal</h3>
          <span>$3,000,000</span>
        </div>
        <div className="Overview-cell">
          <h3 className="Overview-title">Actual</h3>
          <span>$6,666,666</span>
        </div>
      </div>
    </div>
  </div>
);

export default BoxOfficeOverview;
