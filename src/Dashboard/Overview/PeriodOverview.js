import React from 'react';
import SVG from 'react-inlinesvg';

import Arrow from '../arrow.svg';

const PeriodOverview = ({ periodIndex }) => (
  <div className="Dashboard-row">
    <h2 className="Dashboard-rowTitle">Week {periodIndex}</h2>
    <div className="Card Card--rounded">
      <div className="Overview Overview--period">
        <div className="Overview-cell Overview-cell--main">
          <h3 className="Overview-title">Score</h3>
          <span>$3.5M</span>
          <div className="Overview-trend Overview-trend--positive">
            <SVG src={Arrow} />
            +0.2
          </div>
        </div>
        <div className="Overview-delimiter Overview-delimiter--vertical" />
        <div className="Overview-row">
          <div className="Overview-cell">
            <h3 className="Overview-title">Budget</h3>
            <span>$2.5M</span>
            <div className="Overview-trend">
              12%
            </div>
          </div>
          <div className="Overview-delimiter Overview-delimiter--vertical" />
          <div className="Overview-cell">
            <h3 className="Overview-title">Reach</h3>
            <span>8M</span>
            <div className="Overview-trend Overview-trend--positive">
              <SVG src={Arrow} />
              +2M
            </div>
          </div>
          <div className="Overview-delimiter Overview-delimiter--vertical" />
          <div className="Overview-cell">
            <h3 className="Overview-title">Frequency</h3>
            <span>6x</span>
            <div className="Overview-trend Overview-trend--positive">
              <SVG src={Arrow} />
              +0.52
            </div>
          </div>
          <div className="Overview-delimiter" />
          <div className="Overview-cell">
            <h3 className="Overview-title">Budget</h3>
            <span>$2.5M</span>
            <div className="Overview-trend Overview-trend--positive">
              <SVG src={Arrow} />
              +29M
            </div>
          </div>
          <div className="Overview-delimiter Overview-delimiter--vertical" />
          <div className="Overview-cell">
            <h3 className="Overview-title">Reach</h3>
            <span>8M</span>
            <div className="Overview-trend Overview-trend--negative">
              <SVG src={Arrow} />
              -0.06
            </div>
          </div>
          <div className="Overview-delimiter Overview-delimiter--vertical" />
          <div className="Overview-cell">
            <h3 className="Overview-title">CPV</h3>
            <span>6x</span>
            <div className="Overview-trend Overview-trend--negative">
              <SVG src={Arrow} />
              +$0.002
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default PeriodOverview;
