import React from 'react';

const CampaignOverview = () => (
  <div className="Dashboard-row">
    <h2 className="Dashboard-rowTitle Dashboard-rowTitle--success">Campaign start</h2>
    <div className="Card Card--rounded">
      <div className="Overview">
        <div className="Overview-cell Overview-cell--main">
          <h3 className="Overview-title">Box office goal</h3>
          <span>$3.5M</span>
        </div>
        <div className="Overview-delimiter" />
        <div className="Overview-cell">
          <h3 className="Overview-title">Budget</h3>
          <span>$2.5M</span>
        </div>
        <div className="Overview-delimiter" />
        <div className="Overview-cell">
          <h3 className="Overview-title">Reach</h3>
          <span>8M</span>
        </div>
        <div className="Overview-delimiter" />
        <div className="Overview-cell">
          <h3 className="Overview-title">Frequency</h3>
          <span>6x</span>
        </div>
      </div>
    </div>
  </div>
);

export default CampaignOverview;
