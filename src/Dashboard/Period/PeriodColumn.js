import React from 'react';
import { connect } from 'react-redux';

import './Notes.styl';
import PeriodOverview from '../Overview/PeriodOverview';
import CreativeTable from './CreativeTable/CreativeTable';
import Report from '../Report/Report';

import { editNote } from './actions';

const PeriodColumn = ({ periodIndex, note, onNoteChange }) => {
  const getRandomTables = () => {
    const tables = [];

    for (let i = 0; i < periodIndex; i++) {
      tables.push(<CreativeTable key={`${periodIndex}-${i}`} />)
    }

    return tables;
  };

  const handleNoteChange = event => onNoteChange(event.target.value);

  return (
    <div className="Dashboard-column">
      <PeriodOverview periodIndex={periodIndex} />
      <div className="Dashboard-row">
        <h2 className="Dashboard-rowTitle">Reports</h2>
        <Report/>
      </div>
      <div className="Dashboard-row">
        <h2 className="Dashboard-rowTitle">Notes</h2>
        <div className="Card Card--rounded Card--transparent">
          <textarea className="Notes" placeholder="Add notes" value={note} onChange={handleNoteChange}/>
        </div>
      </div>
      <div className="Dashboard-row">
        <h2 className="Dashboard-rowTitle">Creatives</h2>
        {getRandomTables()}
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  note: state.notes[ownProps.periodIndex]
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onNoteChange: note => dispatch(editNote(ownProps.periodIndex, note))
});

export default connect(mapStateToProps, mapDispatchToProps)(PeriodColumn);
