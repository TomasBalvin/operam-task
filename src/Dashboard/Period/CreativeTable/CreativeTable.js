import React from 'react';

import './CreativeTable.styl';
import TrailerImg from './trailer-img.png';

const CreativeTable = () => {
  const getRandomRows = () => {
    const rows = [];

    for (let i = 0; i < 3; i++) {
      rows.push(
        <tr key={i}>
          <td>
            <div className="CreativeTable-description">
              <img src={TrailerImg} alt="" className="CreativeTable-itemImg"/>
              <div>
                <h4 className="CreativeTable-typeTitle">Trailer</h4>
                <span>Extended trailer</span>
              </div>
            </div>
          </td>
          <td>289K</td>
          <td>1.2</td>
          <td>21%</td>
          <td>$0.008</td>
        </tr>
      );
    }

    return rows;
  };

  return (
    <div className="Card">
      <table className="CreativeTable">
        <thead>
          <tr>
            <th>Facebook</th>
            <th>IMP</th>
            <th>PSR</th>
            <th>Views</th>
            <th>CPV</th>
          </tr>
        </thead>
        <tbody>
          {getRandomRows()}
        </tbody>
      </table>
    </div>
  );
};

export default CreativeTable;
