import { EDIT_NOTE } from './actions';

export const notesReducer = (state = {}, action) => {
  switch (action.type) {
    case EDIT_NOTE:
      return {
        ...state,
        [action.periodIndex]: action.note
      };
    default:
      return state;
  }
};
