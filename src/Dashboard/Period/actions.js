export const EDIT_NOTE = 'EDIT_NOTE';
export const editNote = (periodIndex, note) => ({
  type: EDIT_NOTE,
  periodIndex,
  note
});
