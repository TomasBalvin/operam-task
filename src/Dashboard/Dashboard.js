import React from 'react';

import './Dashboard.styl';
import './Card.styl';
import './Overview/Overview.styl';

import CampaignOverview from './Overview/CampaignOverview';
import PeriodColumn from './Period/PeriodColumn';
import Report from './Report/Report';
import BoxOfficeOverview from './Overview/BoxOfficeOverview';

const Dashboard = () => {
  const getPeriodColumns = () => {
    const columns = [];

    for (let i = 13; i > 0; i--) {
      columns.push(<PeriodColumn key={i} periodIndex={i} />)
    }

    return columns;
  };

  return (
    <div className="Dashboard">
      <div className="Dashboard-column">
        <CampaignOverview/>
      </div>
      {getPeriodColumns()}
      <div className="Dashboard-column">
        <BoxOfficeOverview/>
        <div className="Dashboard-row">
          <h2 className="Dashboard-rowTitle">Reports</h2>
          <Report/>
          <Report/>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
