import React from 'react';
import SVG from 'react-inlinesvg';

import './Report.styl';
import File from './file.svg';
import Arrow from '../arrow.svg';

const Report = () => {
  const downloadReport = () => {
    alert('Download report');
  };

  return (
    <div className="Card Card--rounded">
      <div className="Report" onClick={downloadReport}>
        <SVG src={File} className="Report-icon" />
        <span className="Report-text">Weekly reports - 13.xls</span>
        <SVG src={Arrow} className="Report-arrow" />
      </div>
    </div>
  );
};

export default Report;
