import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import './Nav.styl';
import Logo from './logo.svg';

const Nav = () => {
  return (
    <div className="Nav">
      <Link to="/" className="Nav-logo"><img src={Logo} alt=""/></Link>
      <NavLink exact to="/" activeClassName="isActive" className="Nav-button">Dashboard</NavLink>
      <NavLink to="/funnel" activeClassName="isActive" className="Nav-button">Funnel</NavLink>
    </div>
  );
};

export default Nav;
