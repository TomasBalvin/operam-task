import { notesReducer } from '../Dashboard/Period/reducers';
import { funnelReducer } from '../Funnel/reducers';

export default {
  notes: notesReducer,
  funnel: funnelReducer
};
