import React, { Component } from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory'
import { Route } from 'react-router'
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux'

import './App.styl';
import reducers from './reducers';
import BasicLayout from '../Layouts/Basic';
import Dashboard from '../Dashboard/Dashboard';
import Funnel from '../Funnel/Funnel';

const getPersistedState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

const history = createHistory();
const middleware = routerMiddleware(history);
const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  getPersistedState(),
  applyMiddleware(middleware)
);

store.subscribe(() => {
  const state = store.getState();
  const serializedState = JSON.stringify(state);
  localStorage.setItem('state', serializedState);
});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <BasicLayout>
            <Route exact path="/" component={Dashboard} />
            <Route path="/funnel" component={Funnel} />
          </BasicLayout>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
