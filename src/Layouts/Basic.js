import React from 'react';

import './Layout.styl';
import Nav from '../Nav/Nav';

const BasicLayout = (props) => {
  return (
    <div className="Layout">
      <Nav />
      <div className="Layout-content">
        {props.children}
      </div>
    </div>
  );
};

export default BasicLayout;
